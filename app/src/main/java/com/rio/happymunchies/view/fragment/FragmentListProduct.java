package com.rio.happymunchies.view.fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.model.repositories.ProductRepositories;
import com.rio.happymunchies.view.activity.AddProductActivity;
import com.rio.happymunchies.view.activity.DetailProductActivity;
import com.rio.happymunchies.view.activity.MainActivity;
import com.rio.happymunchies.view.adapters.ListProductAdapters;
import com.rio.happymunchies.view.adapters.listeners.RecyclerViewItemClickListeners;
import com.rio.happymunchies.view.utils.RecyclerViewItemDecoration;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class FragmentListProduct extends Fragment implements RecyclerViewItemClickListeners.ListProductAdaptersListener {



    @BindView(R.id.listProduct)
    RecyclerView listProduct;
    @BindView(R.id.txtMenuTitle)
    TextView txtMenuTitle;
    @BindString(R.string.DETAIL_PRODUCT_EXTRA)
    String detailProductExtra;
    @BindString(R.string.produk_list_fragment_argumants_key_main_course)
    String dataArgsKey;
    @BindView(R.id.layout_empty_product)
    LinearLayout emptyProductLayout;
    @BindView(R.id.btnAddNewProduct)
    Button btnAddNewProduct;

    public View view;
    private ArrayList<ProductModel> dataProduk = new ArrayList<>();
    private String category = null;
    ProductRepositories productRepositories;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState){
        view = inflater.inflate(R.layout.fragment_list_product, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        productRepositories = new ProductRepositories(getContext(),(MainActivity)getActivity());
        ButterKnife.bind(this,view);
        initComponent();
    }

    private void initComponent(){
        txtMenuTitle.setText(initDataCategory());
        for(int n=0;n<productRepositories.getProduct().size();n++){
            if(productRepositories.getProduct().get(n).category.equals(initDataCategory())){
                dataProduk.add(productRepositories.getProduct().get(n));
            }
        }
        listProduct.setLayoutManager(new GridLayoutManager(getContext(),3));
        listProduct.addItemDecoration(new RecyclerViewItemDecoration(getContext(),R.dimen.item_decoration_spacing));
        listProduct.setAdapter(new ListProductAdapters(dataProduk,getContext(),this));
        btnAddNewProduct.setOnClickListener(v ->
                startActivity(new Intent(getActivity(), AddProductActivity.class)));
    }

    private String initDataCategory(){
        if(getArguments() != null){
            category = getArguments().getString(dataArgsKey);
        }
        return category;
    }


    @Override
    public void onClickItem(String name) {
        startActivity(new Intent(getActivity(), DetailProductActivity.class)
                .putExtra(detailProductExtra,name));
    }
}
