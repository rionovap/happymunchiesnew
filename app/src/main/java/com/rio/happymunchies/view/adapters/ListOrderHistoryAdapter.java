package com.rio.happymunchies.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderHistoryModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class ListOrderHistoryAdapter extends RecyclerView.Adapter<ListOrderHistoryAdapter.ViewHolder> {

    private ArrayList<OrderHistoryModel> orderHistoryData;
    private Context context;
    private int totalPrice;

    public ListOrderHistoryAdapter(ArrayList<OrderHistoryModel> orderHistoryData, Context context) {
        this.orderHistoryData = orderHistoryData;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history,parent,false);
        return new ListOrderHistoryAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        totalPrice = 0;
        holder.txtNamaPemesan.setText(orderHistoryData.get(position).getOrderPersonName());
        holder.txtNomorMeja.setText(String.format("%s%s", context.getString(R.string.mejaNumber),
                String.valueOf(orderHistoryData.get(position).getOrderTableNumber())));
        holder.txtTanggalPemesanan.setText(orderHistoryData.get(position).getOrderDate());
        if(orderHistoryData.get(position).getOrderType() == 0){
            holder.txtOrderType.setText(context.getString(R.string.orderTypeDineIN));
        }else{
            holder.txtOrderType.setText(context.getString(R.string.orderTypeTakeAway));
        }
        for(int x=0;x<orderHistoryData.get(position).getOrderItems().size();x++){
            totalPrice = totalPrice+(orderHistoryData.get(position).getOrderItems().get(x).getOrderProductPrice()*orderHistoryData.get(position).getOrderItems().get(x).getOrderProductQuantity());
        }
        holder.txtOrderTotalPrice.setText(context.getString(R.string.rupiah)+String.valueOf(totalPrice));
        holder.rvOrderHistoryItemList.setLayoutManager(new LinearLayoutManager(context));
        holder.rvOrderHistoryItemList.setAdapter(new ListOrderHistoryItemAdapter(orderHistoryData.get(position).getOrderItems(), context));
    }

    @Override
    public int getItemCount() {
        return orderHistoryData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNamaPemesan;
        private TextView txtNomorMeja;
        private TextView txtTanggalPemesanan;
        private TextView txtOrderType;
        private TextView txtOrderTotalPrice;
        private RecyclerView rvOrderHistoryItemList;
        ViewHolder(View itemView) {
            super(itemView);
            txtNamaPemesan = itemView.findViewById(R.id.txtOrderHistoryPerson);
            txtNomorMeja = itemView.findViewById(R.id.txtOrderHistoryTableNumber);
            txtTanggalPemesanan = itemView.findViewById(R.id.txtOrderHistoryDate);
            txtOrderType = itemView.findViewById(R.id.txtOrderHistoryOrderType);
            txtOrderTotalPrice = itemView.findViewById(R.id.txtOrderHistoryOrderTotalPrice);
            rvOrderHistoryItemList = itemView.findViewById(R.id.rvOrderHistoryItemList);
        }
    }

}
