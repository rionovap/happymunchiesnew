package com.rio.happymunchies.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import com.rio.happymunchies.DBHelper;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderHistoryModel;
import com.rio.happymunchies.model.repositories.OrderHistoryRepositories;
import com.rio.happymunchies.view.adapters.ListOrderHistoryAdapter;

import java.util.ArrayList;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvHistoryOrderList)
    RecyclerView rvHistoryOrderList;
    @BindView(R.id.btnMenu)
    ImageButton btnMenu;
    @BindView(R.id.btnOrderan)
    ImageButton btnOrderan;


    ArrayList<OrderHistoryModel> orderData = new ArrayList<>();
    OrderHistoryRepositories orderHistoryRepositories;
    DBHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        ButterKnife.bind(this);
        orderHistoryRepositories = new OrderHistoryRepositories(getApplicationContext(),this);
        database = new DBHelper(this);
        initToolbar();
        init();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
        }
    }

    private void init(){
        ArrayList<OrderHistoryModel> historyData = new ArrayList<>(database.getAllHistory());
        btnMenu.setOnClickListener(v->{
            finish();
            startActivity(new Intent(this,MainActivity.class));
        });
        btnOrderan.setOnClickListener(v->{
            finish();
            startActivity(new Intent(this,OrderActivity.class));
        });
        rvHistoryOrderList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        rvHistoryOrderList.setAdapter(new ListOrderHistoryAdapter(historyData,getApplicationContext()));
    }


}
