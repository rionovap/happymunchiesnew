package com.rio.happymunchies.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderModel;
import com.rio.happymunchies.view.adapters.listeners.RecyclerViewItemClickListeners;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListOrderedItemAdapter extends RecyclerView.Adapter<ListOrderedItemAdapter.ViewHolder> {

    private ArrayList<OrderModel> orderData;
    private Context context;
    private RecyclerViewItemClickListeners.ListOrderAdaptersListener listener;

    public ListOrderedItemAdapter(ArrayList<OrderModel> orderData, Context context, RecyclerViewItemClickListeners.ListOrderAdaptersListener listener) {
        this.orderData = orderData;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_orderan,parent,false);
        return new ListOrderedItemAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtNamaItem.setText(orderData.get(position).orderProductName);
        holder.txtQtyItem.setText(String.valueOf(orderData.get(position).orderProductQuantity));
        holder.txtTotalHarga.setText("Rp "+String.valueOf(orderData.get(position).orderProductPrice * orderData.get(position).orderProductQuantity));
        holder.btnMinus.setOnClickListener(v -> {
            listener.onClickMinusItem(position,orderData.get(position));
        });
        holder.btnPlus.setOnClickListener(v ->{
            listener.onClickPlusItem(position, orderData.get(position));
        });
    }

    @Override
    public int getItemCount() {
        return orderData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNamaItem;
        private TextView txtQtyItem;
        private TextView txtTotalHarga;
        private ImageButton btnMinus;
        private ImageButton btnPlus;
        ViewHolder(View itemView) {
            super(itemView);
            txtNamaItem = itemView.findViewById(R.id.txtNamaItemOrderan);
            txtQtyItem = itemView.findViewById(R.id.txtQtyItemOrderan);
            txtTotalHarga = itemView.findViewById(R.id.txtTotalHargaOrderan);
            btnMinus = itemView.findViewById(R.id.btnMinusItem);
            btnPlus = itemView.findViewById(R.id.btnPlusItem);
        }
    }

    public void refresh(ArrayList<OrderModel> newData){
        this.orderData = newData;
        notifyDataSetChanged();
    }

}
