package com.rio.happymunchies.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.Bundle;
import android.util.Base64;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderModel;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.model.repositories.CurrentOrderRepositories;
import com.rio.happymunchies.model.repositories.ProductRepositories;
import com.wajahatkarim3.easymoneywidgets.EasyMoneyTextView;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import androidx.appcompat.widget.Toolbar;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DetailProductActivity extends AppCompatActivity {

    @BindView(R.id.image)
    ImageView detailProductImgView;
    @BindView(R.id.txtDetailProductTitle)
    TextView txtNamaProduk;
    @BindView(R.id.txt_product_category)
    TextView txtCategoryProduk;
    @BindView(R.id.txtDetailHargaProduct)
    EasyMoneyTextView txtHargaProduk;
    @BindView(R.id.txtDetailDescProduct)
    TextView txtDescProduk;
    @BindView(R.id.btnOrderThisProduct)
    Button btnOrderThisProduct;
    @BindString(R.string.DETAIL_PRODUCT_EXTRA)
    String DETAIL_PRODUCT_EXTRA;
    @BindString(R.string.EDIT_PRODUCT_EXTRA)
    String EDIT_PRODUCT_EXTRA;

    private ProductModel productModel;
    private ProductRepositories productRepositories;
    private CurrentOrderRepositories orderRepositories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_product);
        ButterKnife.bind(this);
        Bundle bundle;
        bundle = getIntent().getExtras();
        assert bundle != null;
        String name = bundle.getString(DETAIL_PRODUCT_EXTRA);
        orderRepositories = new CurrentOrderRepositories(getApplicationContext(),this);
        productRepositories = new ProductRepositories(getApplicationContext(), this);
        for(int n=0;n<productRepositories.getProduct().size();n++){
            if(productRepositories.getProduct().get(n).getName().equals(name)){
                productModel = productRepositories.getProduct().get(n);
            }
        }
        initToolbar();
        initComponent();
    }

    @Override protected void onStart(){
        super.onStart();
    }


    /**
     * Initialization toolbar method
     */
    private void initToolbar(){
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if(actionbar != null) {
            actionbar.setDisplayHomeAsUpEnabled(true);
            actionbar.setHomeAsUpIndicator(R.drawable.ic_back_white);
            actionbar.setTitle("");
            toolbar.setNavigationOnClickListener(view ->
                    onBackPressed());
        }
    }

    private void initComponent(){
        txtNamaProduk.setText(productModel.getName());
        txtCategoryProduk.setText(productModel.getCategory());
        txtDescProduk.setText(productModel.getDesc());
        txtHargaProduk.setText(String.valueOf(productModel.getHarga()));
        txtHargaProduk.setCurrency("Rp");
        txtHargaProduk.showCommas();
        txtHargaProduk.showCurrencySymbol();
        Glide.with(getApplicationContext()).load(productModel.getImg()).into(detailProductImgView);
        btnOrderThisProduct.setOnClickListener(view -> {
            orderRepositories.addActiveOrderItem(new OrderModel(productModel.getName(),1, productModel.getHarga()));
            finish();
            startActivity(new Intent(this,OrderActivity.class));
        });
    }



}
