package com.rio.happymunchies.view.activity;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.content.res.AppCompatResources;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.model.repositories.ProductRepositories;
import com.rio.happymunchies.view.fragment.FragmentListProduct;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.fragment_container_main)
    FrameLayout framecontainer;
    @BindString(R.string.produk_list_fragment_argumants_key_main_course)
    String dataArgsKeyMainCourse;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    private Bundle bundle;
    private ActionBarDrawerToggle toggle;
    private FragmentListProduct fragmentListProduct;
    private ProductRepositories productRepositories;
    private ArrayList<ProductModel> productTemp = new ArrayList<>();
    private ArrayList<ProductModel> productFiltered = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_navigation_drawer);
        ButterKnife.bind(this);
        initToolbar();
        init();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        if (actionbar != null) {
            actionbar.setDisplayShowTitleEnabled(false);
            actionbar.setHomeAsUpIndicator(AppCompatResources
                    .getDrawable(getApplicationContext(), R.drawable.ic_menu));
        }
        drawerLayout = findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggle(this,drawerLayout, toolbar,R.string.main_menu_drawer_open,R.string.main_menu_drawer_close);
        toggle.setHomeAsUpIndicator(R.drawable.ic_menu);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void init(){
        productRepositories = new ProductRepositories(getApplicationContext(), this);
        productTemp = new ArrayList<>(productRepositories.getProduct());
        listMainCourse();
    }

    @OnClick(R.id.btnOrderan)
    public void goToOrderan(){
        startActivity(new Intent(this, OrderActivity.class));
    }

    @OnClick(R.id.btnHistory)
    public void goToHistory(){
        startActivity(new Intent(this, HistoryActivity.class));
    }

    @OnClick(R.id.btn_maincourse)
    public void listMainCourse(){
        productFiltered = new ArrayList<>();
        for(int n=0;n<productTemp.size();n++){
            if(productTemp.get(n).getCategory().equals("Main Course")){
                productFiltered.add(productTemp.get(n));
            }
        }
        bundle = new Bundle();
        bundle.putString(dataArgsKeyMainCourse,"Main Course");
        fragmentListProduct = new FragmentListProduct();
        fragmentListProduct.setArguments(bundle);
        loadFragment(fragmentListProduct,framecontainer.getId());
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.btn_snack)
    public void listSnacks(){
        productFiltered = new ArrayList<>();
        for(int n=0;n<productTemp.size();n++){
            if(productTemp.get(n).getCategory().equals("Snacks")){
                productFiltered.add(productTemp.get(n));
            }
        }
        bundle = new Bundle();
        bundle.putString(dataArgsKeyMainCourse,"Snacks");
        fragmentListProduct = new FragmentListProduct();
        fragmentListProduct.setArguments(bundle);
        loadFragment(fragmentListProduct,framecontainer.getId());
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.btn_basics)
    public void listBasics(){
        productFiltered = new ArrayList<>();
        for(int n=0;n<productTemp.size();n++){
            if(productTemp.get(n).getCategory().equals("Basics")){
                productFiltered.add(productTemp.get(n));
            }
        }
        bundle = new Bundle();
        bundle.putString(dataArgsKeyMainCourse,"Basics");
        fragmentListProduct = new FragmentListProduct();
        fragmentListProduct.setArguments(bundle);
        loadFragment(fragmentListProduct,framecontainer.getId());
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.btn_paker)
    public void listPaket(){
        productFiltered = new ArrayList<>();
        for(int n=0;n<productTemp.size();n++){
            if(productTemp.get(n).getCategory().equals("Paket Munchies")){
                productFiltered.add(productTemp.get(n));
            }
        }
        bundle = new Bundle();
        bundle.putString(dataArgsKeyMainCourse,"Paket Munchies");
        fragmentListProduct = new FragmentListProduct();
        fragmentListProduct.setArguments(bundle);
        loadFragment(fragmentListProduct,framecontainer.getId());
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    @OnClick(R.id.btn_drinks)
    public void listDrinks(){
        productFiltered = new ArrayList<>();
        for(int n=0;n<productTemp.size();n++){
            if(productTemp.get(n).getCategory().equals("Drinks")){
                productFiltered.add(productTemp.get(n));
            }
        }
        bundle = new Bundle();
        bundle.putString(dataArgsKeyMainCourse,"Drinks");
        fragmentListProduct = new FragmentListProduct();
        fragmentListProduct.setArguments(bundle);
        loadFragment(fragmentListProduct,framecontainer.getId());
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private void loadFragment(Fragment fragment, int id) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(id, fragment);
        fragmentTransaction.commit();
    }


}
