package com.rio.happymunchies.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.model.repositories.ProductRepositories;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {

    ProductRepositories productRepositories;
    ArrayList<ProductModel> productModels;

    /**
     * Initialization activity lifecycle
     *
     * @param savedInstanceState Reference to a Bundle object
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //set content view
        setContentView(R.layout.activity_splash);
        //initialization
        init();
    }

    /**
     * Initialization method
     */

    private void init() {
        productModels = new ArrayList<>();
        productModels.add(new ProductModel("Sate Lilit Ayam", "https://i.imgur.com/rjrNPG3.jpg",10000, getResources().getString(R.string.desc_sate_lilit_ayam), "Main Course"  ));
        productModels.add(new ProductModel("Sate Lilit Tuna", "https://i.imgur.com/Jsc49FK.jpg",11000, getResources().getString(R.string.desc_sate_lilit_tuna), "Main Course"  ));
        productModels.add(new ProductModel("Kabobs Satay Sapi","https://i.imgur.com/Gp7BV7y.jpg",12000,getResources().getString(R.string.desc_kabobs_satay_sapi),"Main Course"));
        productModels.add(new ProductModel("Kabobs Satay Ayam","https://i.imgur.com/6IlmRCG.jpg",10000,getResources().getString(R.string.desc_kabobs_satay_ayam),"Main Course"));
        productModels.add(new ProductModel("Kerang Bakar Happy Munchies","https://i.imgur.com/jJEvQya.jpg",13000,getResources().getString(R.string.desc_kerang_bakar),"Main Course"));
        productModels.add(new ProductModel("Rice Bowl BBQ Meatball","https://i.imgur.com/M3pDjKu.jpg",20000,getResources().getString(R.string.desc_rice_bowl_bbq),"Main Course"));
        productModels.add(new ProductModel("Rice Bowl Daging Balado","https://i.imgur.com/RhGzLzF.jpg",20000,getResources().getString(R.string.desc_rice_bowl_balado),"Main Course"));
        productModels.add(new ProductModel("Rice Bowl Ayam Sambal Mozarella","https://i.imgur.com/91i1Zsn.jpg",20000,getResources().getString(R.string.desc_rice_bowl_mozarella),"Main Course"));
        productModels.add(new ProductModel("Rice Bowl Ayam Cabe Garam","https://i.imgur.com/NznLSZ8.jpg",18000,getResources().getString(R.string.desc_rice_bowl_cabe_garam),"Main Course"));
        productModels.add(new ProductModel("Rice Bowl Telor Sambal Matah","https://i.imgur.com/l4bW0Kr.jpg",16000,getResources().getString(R.string.desc_rice_bowl_matah),"Main Course"));

        productModels.add(new ProductModel("Happy Crunch Sapi","https://i.imgur.com/4N59zlW.jpg",15000,getResources().getString(R.string.desc_happy_crunch_sapi),"Snacks"));
        productModels.add(new ProductModel("Happy Crunch Kulit Ayam","https://i.imgur.com/vo0AMOd.jpg",10000,getResources().getString(R.string.desc_happy_crunch_ayam),"Snacks"));
        productModels.add(new ProductModel("Lumpia Mozarella","https://i.imgur.com/SjgRzI7.jpg",10000,getResources().getString(R.string.desc_lumpia_mozarella),"Snacks"));
        productModels.add(new ProductModel("Onion Ring","https://i.imgur.com/FlmrIq9.jpg",10000,getResources().getString(R.string.desc_onion_ring),"Snacks"));
        productModels.add(new ProductModel("Pisang Blueberry Keju","https://i.imgur.com/Cl8e1w1.jpg",13000,getResources().getString(R.string.desc_pisang_blueberry_keju),"Snacks"));

        productModels.add(new ProductModel("Potato Wedges","https://i.imgur.com/LfaNz9A.jpg",15000,getResources().getString(R.string.desc_potato_wdges),"Basics"));
        productModels.add(new ProductModel("Kentang Goreng","https://i.imgur.com/zM8WhXi.jpg",10000,getResources().getString(R.string.desc_kentang_goreng),"Basics"));
        productModels.add(new ProductModel("Nasi","https://i.imgur.com/AbVgXYz.jpg",5000,getResources().getString(R.string.desc_nasi),"Basics"));

        productModels.add(new ProductModel("Sate Lilit Ayam Nasi","https://i.imgur.com/gbBaHlb.jpg",23000,getResources().getString(R.string.desc_sate_lilit_ayam_nasi),"Paket Munchies"));
        productModels.add(new ProductModel("Sate Lilit Tuna Nasi","https://i.imgur.com/irV6jEx.jpg",27000,getResources().getString(R.string.desc_sate_lilit_tuna_nasi),"Paket Munchies"));
        productModels.add(new ProductModel("Kabobs Satay Sapi Kentang","https://i.imgur.com/6MWotMU.jpg",30000,getResources().getString(R.string.desc_kabobs_satay_sapi_kentang),"Paket Munchies"));
        productModels.add(new ProductModel("Kabobs Satay Ayam Kentang","https://i.imgur.com/u7HmxsT.jpg",27000,getResources().getString(R.string.desc_kabobs_satay_ayam_kentang),"Paket Munchies"));
        productModels.add(new ProductModel("Kerang Nasi","https://i.imgur.com/Ldof0Oi.jpg",16000,getResources().getString(R.string.desc_kerang_nasi),"Paket Munchies"));

        productModels.add(new ProductModel("Happy Aloevera","https://i.imgur.com/jBon4oj.jpg",22000,getResources().getString(R.string.desc_happy_aloevera),"Drinks"));
        productModels.add(new ProductModel("Strawberry Mojito","https://i.imgur.com/V3b001Y.jpg",20000,getResources().getString(R.string.desc_straw_mojito),"Drinks"));
        productModels.add(new ProductModel("Blackcurrant Mojito","https://i.imgur.com/Ji5Zq0P.jpg",18000,getResources().getString(R.string.desc_blackc_mojito),"Drinks"));
        productModels.add(new ProductModel("Marquisa Mojito","https://i.imgur.com/dE399M8.jpg",18000,getResources().getString(R.string.desc_marquisa_mojito),"Drinks"));
        productModels.add(new ProductModel("Mango Mojito","https://i.imgur.com/9ev2RVd.jpg",18000,getResources().getString(R.string.desc_mango_mojito),"Drinks"));
        productModels.add(new ProductModel("Virgin Mojito","https://i.imgur.com/3r2dBKI.jpg",16000,getResources().getString(R.string.desc_virgin_mojito),"Drinks"));


        productRepositories = new ProductRepositories(getApplicationContext(), this);
        productRepositories.addProduct(productModels);
        //binding views
        ButterKnife.bind(this);

        //add delay timer
        new Handler().postDelayed(new SplashRunnable(), 4000);
    }

    /**
     * Splash runnable class
     */
    private class SplashRunnable implements Runnable {
        @Override
        public void run() {
            startActivity(new Intent(SplashActivity.this, MainActivity.class));
        }
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        int width = drawable.getIntrinsicWidth();
        width = width > 0 ? width : 1;
        int height = drawable.getIntrinsicHeight();
        height = height > 0 ? height : 1;

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }
}
