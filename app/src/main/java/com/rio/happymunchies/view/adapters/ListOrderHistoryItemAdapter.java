package com.rio.happymunchies.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderHistoryModel;
import com.rio.happymunchies.model.OrderModel;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ListOrderHistoryItemAdapter extends RecyclerView.Adapter<ListOrderHistoryItemAdapter.ViewHolder>{

    private ArrayList<OrderModel> orderHistoryItemData;
    private Context context;

    public ListOrderHistoryItemAdapter(ArrayList<OrderModel> orderHistoryItemData, Context context) {
        this.orderHistoryItemData = orderHistoryItemData;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_history_item_list,parent,false);
        return new ListOrderHistoryItemAdapter.ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtNamaItem.setText(orderHistoryItemData.get(position).getOrderProductName());
        holder.txtJumlahItem.setText(String.valueOf(orderHistoryItemData.get(position).getOrderProductQuantity()));
        holder.txtTotalHargaItem.setText(context.getString(R.string.rupiah)+String.valueOf(orderHistoryItemData.get(position).getOrderProductQuantity()*orderHistoryItemData.get(position).getOrderProductPrice()));
    }

    @Override
    public int getItemCount() {
        return orderHistoryItemData.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNamaItem;
        private TextView txtJumlahItem;
        private TextView txtTotalHargaItem;
        ViewHolder(View itemView) {
            super(itemView);
            txtNamaItem = itemView.findViewById(R.id.txtNamaItemOrderan);
            txtTotalHargaItem = itemView.findViewById(R.id.txtTotalHargaOrderan);
            txtJumlahItem = itemView.findViewById(R.id.txtQtyItemOrderan);
        }
    }

}
