package com.rio.happymunchies.view.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.model.repositories.ProductRepositories;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;
import com.wajahatkarim3.easymoneywidgets.EasyMoneyEditText;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class AddProductActivity extends AppCompatActivity {

    @BindView(R.id.parent_layout)
    LinearLayout parentLayout;
    @BindView(R.id.addImgProduk)
    ImageView imgViewholder;
    @BindView(R.id.txtNamaProduk)
    EditText txtNamaProduk;
    @BindView(R.id.txtHargaProduk)
    EasyMoneyEditText txtHargaProduk;
    @BindView(R.id.spinnerKategoriProduk)
    MaterialSpinner spinnerKategoriProduk;
    @BindView(R.id.txtDescProduk)
    EditText txtDescProduk;
    @BindView(R.id.txtDescProdukCharIndicator)
    TextView txtDescProdukCharIndicator;
    @BindView(R.id.btnTambahProduk)
    Button btnTambahProduk;
    @BindString(R.string.EDIT_PRODUCT_EXTRA)
    String EDIT_PRODUCT_EXTRA;

    private ArrayList<String>productCategoriesName = new ArrayList<>();
    private ProductRepositories productRepositories;
    private ArrayList<ProductModel> tempData = new ArrayList<>();
    private ArrayList<ProductModel> tempDataNew = new ArrayList<>();

    private ByteArrayOutputStream byteArrayOutputStream;
    public String productImgBase64;
    public String productCategory;
    public boolean hasImage = false;
    public boolean hasName = false;
    public boolean hasPrice = false;
    public boolean hasCategory = false;
    private boolean isEdit = false;
    private ProductModel tempProductModel;
    private String extraProductName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        productRepositories = new ProductRepositories(getApplicationContext(), this);
        if(getIntent().getExtras() != null){
            isEdit = true;
            extraProductName = getIntent().getStringExtra(EDIT_PRODUCT_EXTRA);
            for(int n=0;n<productRepositories.getProduct().size();n++){
                if(productRepositories.getProduct().get(n).getName().equals(extraProductName)){
                    tempProductModel = productRepositories.getProduct().get(n);
                }
            }
        }else{
            isEdit = false;
        }
        initComponent();
    }

    private void initComponent(){
        tempData = productRepositories.getProduct();
        productCategoriesName = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.listCategories)));
        txtHargaProduk.showCommas();
        txtHargaProduk.hideCurrencySymbol();
        spinnerKategoriProduk.setItems(productCategoriesName);
        spinnerKategoriProduk.setOnItemSelectedListener((view, position, id, item) -> {
            productCategory = (String)item;
            hasCategory = true;
        });
        txtDescProduk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                txtDescProdukCharIndicator.setText(String.valueOf(100-txtDescProduk.getText().toString().length()));
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
        txtHargaProduk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    hasPrice = true;
                }else {
                    hasPrice = false;
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
        txtNamaProduk.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(count>0){
                    hasName = true;
                }else {
                    hasName = false;
                }
            }
            @Override
            public void afterTextChanged(Editable s) { }
        });
    }
}
