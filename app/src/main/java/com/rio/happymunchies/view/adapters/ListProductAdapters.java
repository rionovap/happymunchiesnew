package com.rio.happymunchies.view.adapters;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.makeramen.roundedimageview.RoundedImageView;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;
import com.rio.happymunchies.view.adapters.listeners.RecyclerViewItemClickListeners;
import com.wajahatkarim3.easymoneywidgets.EasyMoneyTextView;

import java.util.ArrayList;

public class ListProductAdapters extends RecyclerView.Adapter<ListProductAdapters.ViewHolder> {

    private ArrayList<ProductModel>productData;
    private Context context;
    private RecyclerViewItemClickListeners.ListProductAdaptersListener listener;

    public ListProductAdapters(ArrayList<ProductModel> productData) {
        this.productData = productData;
    }

    public ListProductAdapters(ArrayList<ProductModel> productData,
                               Context context,
                               RecyclerViewItemClickListeners.ListProductAdaptersListener listener){
        this.productData = productData;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_product_card_mode,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
//        String base64Img = productData.get(position).getImg();
//        byte[] decodedBytes = Base64.decode(base64Img, Base64.DEFAULT);
        Glide.with(context).load(productData.get(position).getImg()).into(viewHolder.imgProduct);
        viewHolder.txtHargaProduk.setText(String.valueOf(productData.get(position).getHarga()));
        viewHolder.txtHargaProduk.setCurrency("Rp");
        viewHolder.txtHargaProduk.showCurrencySymbol();
        viewHolder.txtHargaProduk.showCommas();
        viewHolder.txtNamaProduk.setText(productData.get(position).getName());
        viewHolder.itemProduct.setOnClickListener(v -> {
            listener.onClickItem(productData.get(position).getName());
        });

    }

    @Override
    public int getItemCount() {
        return productData.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtNamaProduk;
        private EasyMoneyTextView txtHargaProduk;
        private RoundedImageView imgProduct;
        private RelativeLayout itemProduct;
        ViewHolder(View itemView) {
            super(itemView);
            itemProduct = itemView.findViewById(R.id.itemCardView);
            txtNamaProduk = itemView.findViewById(R.id.txtNamaProduct);
            txtHargaProduk = itemView.findViewById(R.id.txtHargaProduct);
            imgProduct = itemView.findViewById(R.id.imgProduct);
        }
    }

    public void refresh(ArrayList<ProductModel> newData){
        this.productData = newData;
        notifyDataSetChanged();
    }

}
