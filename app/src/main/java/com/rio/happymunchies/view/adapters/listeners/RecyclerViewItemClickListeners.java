package com.rio.happymunchies.view.adapters.listeners;

import com.rio.happymunchies.model.OrderModel;
import com.rio.happymunchies.model.ProductModel;

public interface RecyclerViewItemClickListeners {
    interface ListProductAdaptersListener{
        void onClickItem(String name);
    }
    interface ListOrderAdaptersListener{
        void onClickMinusItem(int position, OrderModel orderModel);
        void onClickPlusItem(int position, OrderModel orderModel);
    }
}
