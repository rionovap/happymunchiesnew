package com.rio.happymunchies.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.rio.happymunchies.DBHelper;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderHistoryModel;
import com.rio.happymunchies.model.OrderModel;
import com.rio.happymunchies.model.repositories.CurrentOrderRepositories;
import com.rio.happymunchies.model.repositories.OrderHistoryRepositories;
import com.rio.happymunchies.view.adapters.ListOrderedItemAdapter;
import com.rio.happymunchies.view.adapters.listeners.RecyclerViewItemClickListeners;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrderActivity extends AppCompatActivity implements RecyclerViewItemClickListeners.ListOrderAdaptersListener {

    @BindView(R.id.rvOrderList)
    RecyclerView rvOrderList;
    @BindView(R.id.btnMenu)
    ImageButton btnMenu;
    @BindView(R.id.btnHistory)
    ImageButton btnHistory;
    @BindView(R.id.layoutEmptyOrder)
    LinearLayout layoutEmptyOrder;
    @BindView(R.id.btnAddNewOrder)
    Button btnAddNewOrder;
    @BindView(R.id.txtOrderPersonName)
    TextView txtOrderPersonName;
    @BindView(R.id.txtTableNumber)
    TextView txtOrderTableNumber;
    @BindView(R.id.txtTotalPriceOrderan)
    TextView txtTotalPriceOrderan;
    @BindView(R.id.radioGroupOrderType)
    RadioGroup radioGroupOrderType;
    @BindView(R.id.btnCheckoutAndSaveOrder)
    Button btnCheckoutAndSaveOrder;

    Boolean nameIsFIlled = false;
    Boolean tableNumberIsFilled = false;
    int orderType = 0;
    int orderPriceTotal = 0;

    OrderHistoryRepositories orderHistoryRepositories;
    CurrentOrderRepositories orderRepositories;
    ArrayList<OrderModel> orderData = new ArrayList<>();
    ListOrderedItemAdapter adapter;
    DBHelper database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order);
        ButterKnife.bind(this);
        database = new DBHelper(this);
//        initToolbar();
        init();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        orderHistoryRepositories = new OrderHistoryRepositories(getApplicationContext(), this);
        orderRepositories = new CurrentOrderRepositories(getApplicationContext(), this);
        orderData = orderRepositories.getActiveOrder();
        txtOrderPersonName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                nameIsFIlled = count > 0;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                nameIsFIlled = count > 0;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        txtOrderTableNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                tableNumberIsFilled = count > 0;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                tableNumberIsFilled = count > 0;
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        radioGroupOrderType.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.radioDineIN) {
                orderType = 0;
            } else {
                orderType = 1;
            }
        });
        for (int x = 0; x < orderData.size(); x++) {
            orderPriceTotal = (orderData.get(x).getOrderProductPrice() * orderData.get(x).getOrderProductQuantity()) + orderPriceTotal;
        }
        txtTotalPriceOrderan.setText(getString(R.string.rupiah) + String.valueOf(orderPriceTotal));
        if (orderData.size() > 0) {
            layoutEmptyOrder.setVisibility(View.GONE);
            adapter = new ListOrderedItemAdapter(orderData, getApplicationContext(), this);
            rvOrderList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            rvOrderList.setAdapter(adapter);
        }
        btnAddNewOrder.setOnClickListener(v -> {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        });

        btnMenu.setOnClickListener(v -> {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        });

        btnHistory.setOnClickListener(v->{
            startActivity(new Intent(this, HistoryActivity.class));
        });

        btnCheckoutAndSaveOrder.setOnClickListener(v -> {
            @SuppressLint("SimpleDateFormat")
            String orderDate = new SimpleDateFormat(getString(R.string.datePatternOrder)).format(new DateTime().toDate());
            if (nameIsFIlled && tableNumberIsFilled && orderRepositories.getActiveOrder().size() > 0) {
                ArrayList<OrderHistoryModel> addToHistory = orderHistoryRepositories.getOrderHistory();
                database.insertHistory(new OrderHistoryModel(
                        txtOrderPersonName.getText().toString(),
                        orderType,
                        orderDate,
                        Integer.parseInt(txtOrderTableNumber.getText().toString()),
                        orderRepositories.getActiveOrder()));
//                addToHistory.add(new OrderHistoryModel(
//                        txtOrderPersonName.getText().toString(),
//                        orderType,
//                        orderDate,
//                        Integer.parseInt(txtOrderTableNumber.getText().toString()),
//                        orderRepositories.getActiveOrder()));
//                orderHistoryRepositories.setOrderHistory(addToHistory);
                orderRepositories.clearOrderItem();
                Toast.makeText(getApplicationContext(), getString(R.string.checkoutSuccess), Toast.LENGTH_SHORT).show();
                finish();
                startActivity(new Intent(this,MainActivity.class));
            }else if(!nameIsFIlled){
                Toast.makeText(getApplicationContext(), getString(R.string.emptyNameField), Toast.LENGTH_SHORT).show();
            }else if(!tableNumberIsFilled){
                Toast.makeText(getApplicationContext(), getString(R.string.emptyTableNumberField), Toast.LENGTH_SHORT).show();
            }else if(orderRepositories.getActiveOrder().size() == 0){
                Toast.makeText(getApplicationContext(), getString(R.string.emptyCartWarning), Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(getApplicationContext(), getString(R.string.error), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @SuppressLint("SetTextI18n")
    @Override
    public void onClickMinusItem(int position, OrderModel orderModel) {
        int newOrderTotalPrice = 0;
        ArrayList<OrderModel> newDataSet = orderRepositories.deleteActiveOrderItem(orderModel);
        adapter.refresh(newDataSet);
        if (newDataSet.size() < 1) {
            layoutEmptyOrder.setVisibility(View.VISIBLE);
            txtTotalPriceOrderan.setText(getString(R.string.rupiah) + String.valueOf(0));
        } else {
            for (int x = 0; x < newDataSet.size(); x++) {
                newOrderTotalPrice = (newDataSet.get(x).getOrderProductPrice() * newDataSet.get(x).getOrderProductQuantity()) + newOrderTotalPrice;
            }
            txtTotalPriceOrderan.setText(getString(R.string.rupiah) + String.valueOf(newOrderTotalPrice));
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClickPlusItem(int position, OrderModel orderModel) {
        int newOrderTotalPrice = 0;
        orderRepositories.addActiveOrderItem(new OrderModel(orderModel.getOrderProductName(),1, orderModel.getOrderProductPrice()));
        ArrayList<OrderModel> newDataSet = orderRepositories.getActiveOrder();
        adapter.refresh(newDataSet);
        if(newDataSet.size() == 0){
            layoutEmptyOrder.setVisibility(View.GONE);
            txtTotalPriceOrderan.setText(getString(R.string.rupiah) + String.valueOf(orderModel.getOrderProductPrice()));
        }else{
            for (int x = 0; x < newDataSet.size(); x++) {
                newOrderTotalPrice = (newDataSet.get(x).getOrderProductPrice() * newDataSet.get(x).getOrderProductQuantity()) + newOrderTotalPrice;
            }
            txtTotalPriceOrderan.setText(getString(R.string.rupiah) + String.valueOf(newOrderTotalPrice));
        }
    }
}
