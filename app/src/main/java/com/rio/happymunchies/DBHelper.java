package com.rio.happymunchies;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.rio.happymunchies.model.OrderHistoryModel;
import com.rio.happymunchies.model.OrderModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class DBHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "history_db";

    public static final String orderHistoryTableName = "OrderHistoryTable";
    public static final String orderPersonName = "OrderPersonName";
    public static final String orderId = "OrderId";
    public static final String orderDate = "OrderDate";
    public static final String orderTableNumber = "OrderTableNumber";
    public static final String orderItem = "OrderItems";
    public static final String orderType = "OrderType";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + orderHistoryTableName + "("
                    + orderId + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + orderPersonName + " TEXT,"
                    + orderDate + " TEXT,"
                    + orderTableNumber + " INTEGER,"
                    + orderItem + " TEXT,"
                    + orderType + " INTEGER"
                    + ")";

    private Gson gson = new Gson();

    public DBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertHistory(OrderHistoryModel orderHistory) {

        // convert arraylist to json obcjet and string,
        // since arraylist cannot be inserted into db



//        JsonArray json = new JsonArray();
//        try {
//            json.put("orderItemArray", new JSONArray(orderHistory.orderItems));
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

        String orderItems = gson.toJson(orderHistory.getOrderItems());

        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(orderPersonName, orderHistory.getOrderPersonName());
        values.put(orderDate, orderHistory.getOrderDate());
        values.put(orderTableNumber, orderHistory.getOrderTableNumber());
        values.put(orderItem, orderItems);
        values.put(orderType, orderHistory.getOrderType());

        // insert row
        db.insert(orderHistoryTableName, null, values);

        // close db connection
        db.close();
    }

    public ArrayList<OrderHistoryModel> getAllHistory() {
        ArrayList<OrderHistoryModel> orderHistoryData = new ArrayList<OrderHistoryModel>();
        ArrayList<OrderModel> finalOutputString = new ArrayList<>();

        String selectQuery = "SELECT  * FROM " + orderHistoryTableName ;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                ArrayList<OrderHistoryModel> itemData = new ArrayList<OrderHistoryModel>();
                OrderHistoryModel data = new OrderHistoryModel();
                try {
                    Type type = new TypeToken<ArrayList<OrderModel>>() {}.getType();
                    String outputArray = cursor.getString(cursor.getColumnIndex(orderItem));
                    finalOutputString = gson.fromJson(outputArray, type);
                } catch (JsonSyntaxException e) {
                    e.printStackTrace();
                }


                data.setOrderPersonName(cursor.getString(cursor.getColumnIndex(orderPersonName)));
                data.setOrderDate(cursor.getString(cursor.getColumnIndex(orderDate)));
                data.setOrderTableNumber(cursor.getInt(cursor.getColumnIndex(orderTableNumber)));
                data.setOrderType(cursor.getInt(cursor.getColumnIndex(orderType)));
                data.setOrderItems(finalOutputString);
                orderHistoryData.add(data);
            } while (cursor.moveToNext());

        }
        db.close();
        return orderHistoryData;
    }

}
