package com.rio.happymunchies.model.repositories;

import android.app.Activity;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderHistoryModel;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.ButterKnife;

public class OrderHistoryRepositories {

    /**
     * Binding resources
     */
    @BindString(R.string.order_history_repo)
    String repoOrderHistory;

    private Context context;

    public OrderHistoryRepositories(Context context, Activity activity) {
        ButterKnife.bind(this, activity);
        this.context = context;
        Hawk.init(context).build();
    }

    public ArrayList<OrderHistoryModel> getOrderHistory(){
        if(Hawk.contains(repoOrderHistory)){
            return Hawk.get(repoOrderHistory);
        }else{
            return new ArrayList<>();
        }
    }

    public void setOrderHistory(ArrayList<OrderHistoryModel> orderHistory){
        Hawk.put(repoOrderHistory, orderHistory);
    }


}
