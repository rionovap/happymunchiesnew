package com.rio.happymunchies.model;

import android.os.Parcel;
import android.os.Parcelable;

public class OrderModel implements Parcelable {

    public String orderProductName;
    public Integer orderProductQuantity;
    public Integer orderProductPrice;


    public OrderModel(String orderProductName, Integer orderProductQuantity, Integer orderProductPrice) {
        this.orderProductName = orderProductName;
        this.orderProductQuantity = orderProductQuantity;
        this.orderProductPrice = orderProductPrice;
    }

    protected OrderModel(Parcel in) {
        orderProductName = in.readString();
        if (in.readByte() == 0) {
            orderProductQuantity = null;
        } else {
            orderProductQuantity = in.readInt();
        }
        if (in.readByte() == 0) {
            orderProductPrice = null;
        } else {
            orderProductPrice = in.readInt();
        }
    }

    public static final Creator<OrderModel> CREATOR = new Creator<OrderModel>() {
        @Override
        public OrderModel createFromParcel(Parcel in) {
            return new OrderModel(in);
        }

        @Override
        public OrderModel[] newArray(int size) {
            return new OrderModel[size];
        }
    };

    public String getOrderProductName() {
        return orderProductName;
    }

    public void setOrderProductName(String orderProductName) {
        this.orderProductName = orderProductName;
    }

    public Integer getOrderProductQuantity() {
        return orderProductQuantity;
    }

    public void setOrderProductQuantity(Integer orderProductQuantity) {
        this.orderProductQuantity = orderProductQuantity;
    }

    public Integer getOrderProductPrice() {
        return orderProductPrice;
    }

    public void setOrderProductPrice(Integer orderProductPrice) {
        this.orderProductPrice = orderProductPrice;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderProductName);
        if (orderProductQuantity == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderProductQuantity);
        }
        if (orderProductPrice == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderProductPrice);
        }
    }
}
