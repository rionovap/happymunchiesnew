package com.rio.happymunchies.model.repositories;

import android.app.Activity;
import android.content.Context;

import com.orhanobut.hawk.Hawk;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.ProductModel;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.ButterKnife;

public class ProductRepositories {

    /**
     * Binding resources
     */
    @BindString(R.string.main_repo_name)
    String repoName;
    @BindString(R.string.product_repo)
    String repoProduct;

    private Context context;

    public ProductRepositories(Context context, Activity activity) {
        ButterKnife.bind(this, activity);
        this.context = context;
        Hawk.init(context).build();
    }

    public void addProduct(ArrayList<ProductModel> productModel){
        Hawk.put(repoProduct, productModel);
    }

    public void addProduct(ProductModel productModel){
        if(Hawk.contains(repoProduct)){
            ArrayList<ProductModel> products = getProduct();
            products.add(productModel);
            Hawk.put(repoProduct, productModel);
        }
    }

    public ArrayList<ProductModel> getProduct(){
        if(Hawk.contains(repoProduct)){
            return Hawk.get(repoProduct);
        }else{
            Hawk.put(repoProduct,new ArrayList<ProductModel>());
            return new ArrayList<>();
        }
    }







}
