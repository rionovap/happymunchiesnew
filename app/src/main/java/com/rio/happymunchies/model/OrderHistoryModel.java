package com.rio.happymunchies.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class OrderHistoryModel implements Parcelable {

    public String orderPersonName;
    public Integer orderType;
    public String orderDate;
    public Integer orderTableNumber;
    public ArrayList<OrderModel> orderItems;

    public OrderHistoryModel() {
    }

    public OrderHistoryModel(String orderPersonName, Integer orderType, String orderDate, Integer orderTableNumber, ArrayList<OrderModel> orderItems) {
        this.orderPersonName = orderPersonName;
        this.orderType = orderType;
        this.orderDate = orderDate;
        this.orderTableNumber = orderTableNumber;
        this.orderItems = orderItems;
    }

    protected OrderHistoryModel(Parcel in) {
        orderPersonName = in.readString();
        if (in.readByte() == 0) {
            orderType = null;
        } else {
            orderType = in.readInt();
        }
        orderDate = in.readString();
        if (in.readByte() == 0) {
            orderTableNumber = null;
        } else {
            orderTableNumber = in.readInt();
        }
        orderItems = in.createTypedArrayList(OrderModel.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(orderPersonName);
        if (orderType == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderType);
        }
        dest.writeString(orderDate);
        if (orderTableNumber == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(orderTableNumber);
        }
        dest.writeTypedList(orderItems);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<OrderHistoryModel> CREATOR = new Creator<OrderHistoryModel>() {
        @Override
        public OrderHistoryModel createFromParcel(Parcel in) {
            return new OrderHistoryModel(in);
        }

        @Override
        public OrderHistoryModel[] newArray(int size) {
            return new OrderHistoryModel[size];
        }
    };

    public String getOrderPersonName() {
        return orderPersonName;
    }

    public void setOrderPersonName(String orderPersonName) {
        this.orderPersonName = orderPersonName;
    }

    public Integer getOrderType() {
        return orderType;
    }

    public void setOrderType(Integer orderType) {
        this.orderType = orderType;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getOrderTableNumber() {
        return orderTableNumber;
    }

    public void setOrderTableNumber(Integer orderTableNumber) {
        this.orderTableNumber = orderTableNumber;
    }

    public ArrayList<OrderModel> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(ArrayList<OrderModel> orderItems) {
        this.orderItems = orderItems;
    }




}
