package com.rio.happymunchies.model.repositories;

import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.orhanobut.hawk.Hawk;
import com.rio.happymunchies.R;
import com.rio.happymunchies.model.OrderModel;

import java.util.ArrayList;

import butterknife.BindString;
import butterknife.ButterKnife;

public class CurrentOrderRepositories {

    /**
     * Binding resources
     */
    @BindString(R.string.current_order_repo)
    String repoCurrentOrder;

    private Context context;

    public CurrentOrderRepositories(Context context, Activity activity) {
        ButterKnife.bind(this, activity);
        this.context = context;
        Hawk.init(context).build();
    }

    public void addActiveOrder(ArrayList<OrderModel> orderModel){
        Hawk.put(repoCurrentOrder, orderModel);
    }

    public void addActiveOrderItem(OrderModel orderModel){
        boolean isAdded = false;
        if(Hawk.contains(repoCurrentOrder)){
            ArrayList<OrderModel> activeOrder = Hawk.get(repoCurrentOrder);
            for(int x=0;x<activeOrder.size();x++){
                if(orderModel.orderProductName.equals(activeOrder.get(x).orderProductName) && !isAdded){
                    activeOrder.get(x).setOrderProductQuantity(activeOrder.get(x).getOrderProductQuantity()+1);
                    addActiveOrder(activeOrder);
                    isAdded = true;
                    Toast.makeText(context,"Produk berhasil dimasukkan ke keranjang", Toast.LENGTH_SHORT).show();
                }
            }
            if(!isAdded){
                activeOrder.add(orderModel);
                addActiveOrder(activeOrder);
                Toast.makeText(context,"Produk berhasil dimasukkan ke keranjang", Toast.LENGTH_SHORT).show();
            }
        }else{
            addActiveOrder(new ArrayList<OrderModel>());
            ArrayList<OrderModel> activeOrder = Hawk.get(repoCurrentOrder);
            activeOrder.add(orderModel);
            addActiveOrder(activeOrder);
            Toast.makeText(context,"Produk berhasil dimasukkan ke keranjang", Toast.LENGTH_SHORT).show();
        }
    }



    public ArrayList<OrderModel> deleteActiveOrderItem(OrderModel orderModel) {
        boolean isDeleted = false;
        if(Hawk.contains(repoCurrentOrder)){
            ArrayList<OrderModel> activeOrder = Hawk.get(repoCurrentOrder);
            for(int x=0;x<activeOrder.size();x++){
                if(orderModel.orderProductName.equals(activeOrder.get(x).orderProductName) && !isDeleted){
                    if(activeOrder.get(x).getOrderProductQuantity() == 1 ){
                        activeOrder.remove(x);
                        addActiveOrder(activeOrder);
                        isDeleted = true;
                    }else{
                        activeOrder.get(x).setOrderProductQuantity(activeOrder.get(x).getOrderProductQuantity()-1);
                        addActiveOrder(activeOrder);
                        isDeleted = true;
                    }
                }
            }
        }
        return getActiveOrder();
    }

    public void clearOrderItem(){
        if(Hawk.contains(repoCurrentOrder)){
            Hawk.delete(repoCurrentOrder);
        }
    }

    public ArrayList<OrderModel> getActiveOrder(){
        if(Hawk.contains(repoCurrentOrder)){
            return Hawk.get(repoCurrentOrder);
        }else{
            return new ArrayList<>();
        }
    }





}
